package ch.gma.docadvisor.docadvisor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocadvisorApplication {

	public static void main(String[] args) {
		SpringApplication.run(DocadvisorApplication.class, args);
	}

}
