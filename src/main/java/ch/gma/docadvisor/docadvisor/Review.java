package ch.gma.docadvisor.docadvisor;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Review {

  private int id;
  private int idClient;
  private int idDoctor;

  private String review;

  private int stars;

  private String date;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getIdClient() {
    return idClient;
  }

  public void setIdClient(int idClient) {
    this.idClient = idClient;
  }

  public int getIdDoctor() {
    return idDoctor;
  }

  public void setIdDoctor(int idDoctor) {
    this.idDoctor = idDoctor;
  }

  public String getReview() {
    return review;
  }

  public void setReview(String review) {
    this.review = review;
  }

  public String getDate() {
    LocalDate localDate = LocalDate.now().minusDays((int)(Math.random())*90);
    return localDate.format(DateTimeFormatter.BASIC_ISO_DATE);
  }

  public void setDate(String date) {
    this.date = date;
  }

  public int getStars() {
    return stars;
  }

  public void setStars(int stars) {
    this.stars = stars;
  }
}
