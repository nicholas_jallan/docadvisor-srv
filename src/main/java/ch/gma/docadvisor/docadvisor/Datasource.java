package ch.gma.docadvisor.docadvisor;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Datasource {

  @Autowired
  private CsvUtils csvUtils;


  private List<Review> reviews;
  private List<Client> clients;
  private List<Doctor> doctors;

  @PostConstruct
  private void init() {
    doctors = csvUtils.loadObjectList(Doctor.class, "doctors.csv");
    clients = csvUtils.loadObjectList(Client.class, "names.csv");
    reviews = csvUtils.loadObjectList(Review.class, "reviews.csv");


  }

  public List<Review> getReviews() {
    return (List<Review>) new ArrayList<>(reviews).clone();
  }

  public List<Client> getClients() {
    return (List<Client>) new ArrayList<>(clients).clone();
  }

  public List<Doctor> getDoctors() {
    return (List<Doctor>) new ArrayList<>(doctors).clone();
  }
}
