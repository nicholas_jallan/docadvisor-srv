package ch.gma.docadvisor.docadvisor;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
public class MainResource {

  @Autowired
  private Datasource ds;

  @RequestMapping(path = "/doctors", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity getDoctors(
    @RequestParam(required = false) String specialite,
    @RequestParam(required = false) String nom){

    List<Doctor> doctors = ds.getDoctors();

    if (!StringUtils.isEmpty(specialite)) doctors = doctors
      .stream()
      .filter(doctor -> doctor.getSpecialite().contains(specialite))
      .collect(Collectors.toList());

    if (!StringUtils.isEmpty(nom)) doctors = doctors
      .stream()
      .filter(doctor -> doctor.getNom().contains(nom))
      .collect(Collectors.toList());

    return ResponseEntity.ok(doctors);

  }

  @RequestMapping(path = "clients", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity getClients(@RequestParam(required = false) String nickname){

    List<Client> clients = ds.getClients();

    if (!StringUtils.isEmpty(nickname)) clients = clients
      .stream()
      .filter(client -> client.getNickname().contains(nickname))
      .collect(Collectors.toList());

    return ResponseEntity.ok(clients);

  }

  @RequestMapping(path = "clients/{id}/reviews", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity getClientReviews(@PathVariable(name="id") int id){

    List<Review> collect = ds
      .getReviews()
      .stream()
      .filter(review -> review.getIdClient() == id)
      .collect(Collectors.toList());

    return ResponseEntity.ok(collect);

  }


  @RequestMapping(path = "reviews", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity getReviews(
    @RequestParam(required = false) Integer minStar,
    @RequestParam(required = false) Integer idDoc,
    @RequestParam(required = false) Integer idClient){

    List<Review> reviews = ds.getReviews();

    if (minStar != null) reviews = reviews
      .stream()
      .filter(review -> review.getStars()>minStar)
      .collect(Collectors.toList());
    if (idClient != null) reviews = reviews
      .stream()
      .filter(review -> review.getIdClient() == idClient)
      .collect(Collectors.toList());
    if (idDoc != null) reviews = reviews
      .stream()
      .filter(review -> review.getIdDoctor() == idDoc)
      .collect(Collectors.toList());

    if (reviews.size() > 100) reviews = reviews.subList(0,100);
    return ResponseEntity.ok(reviews);

  }

}
